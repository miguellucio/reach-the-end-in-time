import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MiniGameComponent } from './mini-game/mini-game.component';

const routes: Routes = [
  {path: 'ReachTheEndInTime', component: MiniGameComponent},
  {path: '**', pathMatch: 'full', redirectTo: 'ReachTheEndInTime'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
