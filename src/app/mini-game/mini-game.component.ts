import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-mini-game',
  templateUrl: './mini-game.component.html',
  styleUrls: ['./mini-game.component.css']
})
export class MiniGameComponent implements OnInit {
  contador:any = 0;


  p1:string[] = ['.'];
  p2:string[] = ['.'];
  p3:string[] = ['.'];
  p4:string[] = ['#'];
  p5:string[] = ['#'];
  p6:string[] = ['.'];
  p7:string[] = ['#'];
  p8:string[] = ['.'];
  p9:string[] = ['.'];

  temp:string[] = [];

  constructor() { 
    
  }

  ngOnInit(): void {
  }


  save(form:NgForm){
    if ( form.valid ){
      this.contador = this.contador + form.value.movimientos;
      form.reset();
      console.log(this.contador);
    }else{
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Ingresa un numero',
      })
    }
    

  }


  drop(event: CdkDragDrop<string[]>) {
   
    if (this.contador != 0){
      if (event.previousContainer === event.container) {
        moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      } else {
        transferArrayItem(event.previousContainer.data,
                          event.container.data,
                          event.previousIndex,
                          event.currentIndex);
      }
      this.contador = this.contador - 1;

      if(this.p3[0] === '#' && this.p4[0] === '#' &&this.p6[0] === '#' && this.p7[0] === '.' && this.p5[0] === '.' && this.p2[0] === '.' && this.p8[0] === '.' && this.p9[0] === '.'){
        Swal.fire({
          icon: 'success',
          title: 'Felicidades',
          text: 'Has llegado felicidades',
        })
      }

      if(this.p3[0] === '#' && this.p4[0] === '#' &&this.p6[0] === '.' && this.p7[0] === '#' && this.p5[0] === '.' && this.p2[0] === '.' && this.p8[0] === '.' && this.p9[0] === '.'){
        Swal.fire({
          icon: 'success',
          title: 'Felicidades',
          text: 'Has llegado felicidades',
        })
      }


      if(this.p3[0] === '.' && this.p4[0] === '#' &&this.p6[0] === '#' && this.p7[0] === '#' && this.p5[0] === '.' && this.p2[0] === '.' && this.p8[0] === '.' && this.p9[0] === '.'){
        Swal.fire({
          icon: 'success',
          title: 'Felicidades',
          text: 'Has llegado felicidades',
        })
      }


      if(this.p3[0] === '.' && this.p4[0] === '#' &&this.p6[0] === ',' && this.p7[0] === '#' && this.p5[0] === '#' && this.p2[0] === '.' && this.p8[0] === '.' && this.p9[0] === '.'){
        Swal.fire({
          icon: 'success',
          title: 'Felicidades',
          text: 'Has llegado felicidades',
        })
      }


    }else{
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'No fue posible llegar',
      })
    }
    
  }

  
}



//soluciones para el ejercicio

//solucion 1  ********
//.    .    #
//#    .    #
//.    .    .


//solucion 2 *********
//.    .    #
//#    .    .
//#    .    .


//solucion 3**********
//.    .    .
//#    .    #
//#    .    .


//solucion 4
//.    .    .
//#    #    .
//#    .    .