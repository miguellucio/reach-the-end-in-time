import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MiniGameComponent } from './mini-game/mini-game.component';
import {DragDropModule } from '@angular/cdk/drag-drop';
import {FormsModule} from '@angular/forms';
@NgModule({
  declarations: [
    AppComponent,
    MiniGameComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DragDropModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
